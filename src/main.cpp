#include "AnalogSensor.h"

AnalogSensor gas;

float GasConvertion(int value)
{
}

void OnGasEvent(int value)
{
  Serial.println(value);
}

void setup()
{
  Serial.begin(9600);
  delay(100);

  //gas.begin(2, 1000, true, OnGasEvent, GasConvertion);

  // todo usado en el setup
  //gas.begin(A0, 1000, true, OnGasEvent);

  // se puede cambiar el callback
  gas.begin(A0,100);
  // gas.notifyOnChange(OnGasEvent, 3);
  // avisa si el valor cambia por lo menos 3 bits,
  // igual se hace un pooling de 1 segundo.

  // o avisa cada N segundos (siempre)
  // gas.notifyEveryMs(OnGasEvent, 1000);

  // solo avisa si el valor cambia (3 bits) y esta dentro de la ventana 100/900
  gas.notifyOnChangeWithin(OnGasEvent, 100, 300, 3);

  //gas.setModoDiagnostico();
}

void loop()
{
  gas.loop();
}