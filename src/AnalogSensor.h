/**
 * Micro57
 * Javier Rambaldo
 * 2021
 * 
 * AnalogSensor.
 * 
 * Intenta ser una clase para leer sensores analógicos con eventos de cambios de valores, etc.
 * 
 */

#pragma once
#include <Arduino.h>

typedef void (*SensorEvent)(int value);

template <typename T>
static inline T DiferenciaAbsoluta(T a, T b) { return a > b ? a - b : b - a; }

class AnalogSensor
{
private:
    uint8_t inputPIN;     // pin donde está conectado el sensor
    bool modoDiagnostico; // habilita cosas de debug
    SensorEvent eventCallback;
    unsigned long ms;
    unsigned long pollingTimeMs; // cada cuanto tiempo refresco el value?
    int adcValue;                // valor del sensor
    int adcLastValue;
    bool notifyOnlyInChanges; // true=solo aviso si cambia el valor, false=avisa siempre cada N ms.
    int offetValue;           // para la ventana de comparacion y evento
    int minValue;
    int maxValue;
    bool windowOpen;

public:
    void begin(uint8_t pin, unsigned long read_every_milisec = 1000, bool notify_only_in_changes = true, SensorEvent event = NULL)
    {
        inputPIN = pin;
        pollingTimeMs = read_every_milisec == 0 ? 1000 : read_every_milisec;
        notifyOnlyInChanges = notify_only_in_changes;
        eventCallback = event;
        ms = millis();
        offetValue = 1;
        minValue = 0;
        maxValue = INT16_MAX;
        windowOpen = false;
    }

    void setModoDiagnostico(bool activarModo = true)
    {
        modoDiagnostico = activarModo;
    }

    // Retorna el ultimo valor leido (a no ser que lo fuerce)
    uint16_t getValue(bool force = false)
    {
        if (force)
            adcValue = analogRead(inputPIN);
        return adcValue;
    }

    // avisa cada vez que cambia el valor
    // (ojo que pisa los valores del begin)
    void notifyOnChange(SensorEvent event, int offet_value = 1)
    {
        offetValue = offet_value;
        eventCallback = event;
        notifyOnlyInChanges = true;
    }

    // lee la entrada cada N milisec y llama al callback
    // (OJO que ya no funciona OnChange!)
    void notifyEveryMs(SensorEvent event, unsigned long ms = 1000)
    {
        eventCallback = event;
        pollingTimeMs = ms;
        notifyOnlyInChanges = false;
    }

    // avisa siempre y cuando el valor cambie con respecto al valor anterior,
    // y que esté dentro de la ventana (min/max)
    void notifyOnChangeWithin(SensorEvent event, int min, int max, int offet_value = 1)
    {
        minValue = min;
        maxValue = max;
        offetValue = offet_value;
        eventCallback = event;
        notifyOnlyInChanges = true;
        windowOpen = true;
    }

    void loop()
    {
        if (millis() - ms > pollingTimeMs)
        {
            // guado el nuevo valor:
            adcValue = analogRead(inputPIN);

            if (modoDiagnostico)
            {
                Serial.print("value=");
                Serial.println(adcValue);
            }

            // si existe evento, puedo avisar?
            if (eventCallback)
            {
                if (!notifyOnlyInChanges)
                    eventCallback(adcValue);

                else if (DiferenciaAbsoluta(adcValue, adcLastValue) >= offetValue)
                {
                    if (!windowOpen)
                        eventCallback(adcValue);

                    else if (adcValue >= minValue && adcValue <= maxValue)
                        eventCallback(adcValue);
                }
            }
            adcLastValue = adcValue;
            ms = millis();
        }
    }
};
